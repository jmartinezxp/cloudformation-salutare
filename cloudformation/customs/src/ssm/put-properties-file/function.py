import os
import boto3
from crhelper import CfnResource
import logging
import traceback
import botocore

logger = logging.getLogger(__name__)

helper = CfnResource(
    json_logging=False,
    log_level='DEBUG',
    boto_level='CRITICAL'
)

ssm = boto3.client('ssm')
s3 = boto3.client('s3')

APL_TAG_STACK_ID = "apl:cloudformation:stack-id"
APL_TAG_STACK_NAME = "apl:cloudformation:stack-name"


def putParameters(Event, Overwrite):
    resource_properties = Event["ResourceProperties"]
    stack_id = Event["StackId"]
    request_type = Event["RequestType"]

    _bucket = os.environ["S3_BUCKET"] #resource_properties["S3Bucket"]
    _key = resource_properties["S3Key"]
    _env = resource_properties["Env"]
    _prefix = resource_properties["Prefix"]
    _name = resource_properties["Name"]
    _profileSeparator = resource_properties["ProfileSeparator"]
    _keyId = resource_properties["KeyId"] if "KeyId" in resource_properties else None
    _tags = resource_properties["Tags"] if "Tags" in resource_properties else []
    # Tags=[
    #     {
    #         'Key': 'string',
    #         'Value': 'string'
    #     },
    # ]

    _tags.append({"Key": APL_TAG_STACK_ID, "Value": stack_id})
    _tags.append({"Key": APL_TAG_STACK_NAME, "Value": stack_id.split("/")[1]})

    _isUpdate = request_type == 'Update'

    try:
        logger.info(f"Getting Object Bucket: {_bucket}, Key: {_key}")
        s3Object = s3.get_object(Bucket=_bucket, Key=_key)
    except botocore.exceptions.ClientError as e:
        if e.response["Error"]["Code"] == 'NoSuchKey' and _isUpdate:
            return
        else:
            raise e

    content = s3Object["Body"].read().decode("utf-8")

    properties = [line for line in content.replace("\r", "").split("\n") if (line and not line.startswith("#"))]

    p_prefix = f'{_prefix}/{_name}-{_env}/'
    i = 0
    for line in properties:
        i += 1
        pair = line.split("=")
        key = pair[0]
        value = pair[1]
        p_name = f'{p_prefix}{key}'.replace("//", "/")
        logger.info(f"Put {p_name}")
        if not _keyId:
            if _isUpdate:
                res = ssm.put_parameter(Name=p_name, Value=value, Type='String', Overwrite=Overwrite, Tier='Standard')
            else:
                res = ssm.put_parameter(Name=p_name, Value=value, Tags=_tags, Type='String', Overwrite=Overwrite,
                                        Tier='Standard')
        else:
            if _isUpdate:
                res = ssm.put_parameter(Name=p_name, Value=value, Type='SecureString', Overwrite=Overwrite,
                                        Tier='Standard', KeyId=_keyId)
            else:
                res = ssm.put_parameter(Name=p_name, Value=value, Tags=_tags, Type='SecureString', Overwrite=Overwrite,
                                        Tier='Standard', KeyId=_keyId)
        logger.debug(res)

    helper.Data.update({"ParametersPrefix": p_prefix.replace("//", "/")})
    # return p_prefix


def handler(event, context):
    helper(event, context)


@helper.create
def create(event, context):
    logger.info("Got Create")
    return putParameters(Event=event, Overwrite=False)


@helper.update
def update(event, context):
    logger.info("Got Update")
    putParameters(Event=event, Overwrite=True)


@helper.delete
def delete(event, context):
    logger.info("Got Delete")

    logger.info(event)
    try:
        resource_properties = event["ResourceProperties"]
        stack_id = event["StackId"]

        _bucket = os.environ["S3_BUCKET"] #resource_properties["S3Bucket"]
        _key = resource_properties["S3Key"]
        _env = resource_properties["Env"]
        _prefix = resource_properties["Prefix"]
        _name = resource_properties["Name"]
        _profileSeparator = resource_properties["ProfileSeparator"]
        _keyId = resource_properties["KeyId"] if "KeyId" in resource_properties else None
        _tags = resource_properties["Tags"] if "Tags" in resource_properties else []

        p_path = f'{_prefix}/{_name}-{_env}/'.replace("//", "/")
        logger.info(f"get parameter on {p_path}")

        r_get_parameters = ssm.get_parameters_by_path(
            Path=p_path,
            Recursive=True
        )
        logger.debug(f"get parameter response")
        logger.debug(r_get_parameters)

        parametros = r_get_parameters["Parameters"]

        while len(parametros) > 0:
            r_del_parameters = ssm.delete_parameters(
                Names=[parameter["Name"] for parameter in parametros]
            )
            logger.debug("delete parameters response")
            logger.debug(r_del_parameters)

            r_get_parameters = ssm.get_parameters_by_path(
                Path=p_path,
                Recursive=True
            )
            logger.debug(f"get parameter response")
            logger.debug(r_get_parameters)

            parametros = r_get_parameters["Parameters"]

    except:
        logger.error(traceback.format_exc())
